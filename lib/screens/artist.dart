import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:imagesearch/widgets/widgets.dart';
import 'package:imagesearch/models/models.dart';
import 'package:imagesearch/services/services.dart';

class Artist extends StatefulWidget {
  Artist({this.user});

  User user;

  @override
  _ArtistState createState() => _ArtistState();
}

class _ArtistState extends State<Artist> {
  User user;
  bool loading = true;
  List images = [];
  int page = 1;
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    user = widget.user;

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        page += 1;
        _getImagesOfArtist();
      }
    });

    _getImagesOfArtist();
  }

  _getImagesOfArtist() {
    getImagesOfArtist(user.username, page: page.toString()).then((res) {
      setState(() {
        images.addAll(res);
        loading = false;
      });
    }).catchError((err) {
      print(err);
    });
  }

  @override
  Widget build(BuildContext context) {
    double padding = 5.0;

    Widget content = loading
        ? LinearProgressIndicator()
        : Padding(
            padding: EdgeInsets.fromLTRB(padding, padding, padding, 0.0),
            child: StaggeredGridView.countBuilder(
              controller: _scrollController,
              crossAxisCount: 2,
              itemCount: images.length,
              itemBuilder: (BuildContext context, int index) {
                return ImageContainer(
                  unsplashImage: UnsplashImage.fromMap(images[index]),
                );
              },
              staggeredTileBuilder: (int index) {
                UnsplashImage unsplashImage =
                    UnsplashImage.fromMap(images[index]);
                double aspectRatio = unsplashImage.height.toDouble() /
                    unsplashImage.width.toDouble();
                double columnWidth = MediaQuery.of(context).size.width / 2;

                return StaggeredTile.extent(1, aspectRatio * columnWidth);
              },
              mainAxisSpacing: padding + 1.0,
              crossAxisSpacing: padding,
            ),
          );

    return Scaffold(
      appBar: AppBar(
        title: Text(user.name),
      ),
      body: content,
    );
  }
}
