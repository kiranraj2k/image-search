import 'dart:convert';

import 'package:http/http.dart' as http;

Future getImages(String query, {String page: "1"}) async {
  final response = await http.get(
      "https://api.unsplash.com/search/photos?page=$page&query=$query",
      headers: {
        "Authorization":
            "Client-ID 421fa5687918efda3adea3928679e78e86890f8d0938361fec97b2d8248ade14",
      });

  final int statusCode = response.statusCode;

  if (statusCode == 201 || statusCode == 200) {
    Map responseBody = json.decode(response.body);

    return responseBody;
  } else {
    throw Exception(json.decode(response.body));
  }
}

Future getImagesOfArtist(String username, {String page: "1"}) async {
  final response = await http.get(
      "https://api.unsplash.com/users/$username/photos?page=$page",
      headers: {
        "Authorization":
        "Client-ID 421fa5687918efda3adea3928679e78e86890f8d0938361fec97b2d8248ade14",
      });

  final int statusCode = response.statusCode;

  if (statusCode == 201 || statusCode == 200) {
    List responseBody = json.decode(response.body);

    return responseBody;
  } else {
    throw Exception(json.decode(response.body));
  }
}
