import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:imagesearch/models/models.dart';

import 'screens/screens.dart';
import 'services/services.dart';
import 'widgets/widgets.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryTextTheme: Theme.of(context).textTheme.apply(
              bodyColor: Colors.pink[500],
              displayColor: Colors.pink[500],
            ),
        backgroundColor: Colors.black,
        accentColor: Colors.pink[500],
        textSelectionHandleColor: Colors.pink[500],
        highlightColor: Colors.pink[500],
        scaffoldBackgroundColor: Colors.black,
        primaryColor: Colors.black,
        splashColor: Colors.pink[500],
        primaryIconTheme: IconThemeData(
          color: Colors.pink[500],
        ),
      ),
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (settings) {
        WidgetBuilder builder;
        Map arguments = settings.arguments;

        switch (settings.name) {
          case '/':
            builder = (
              BuildContext _,
            ) =>
                MyHomePage();
            break;
          case '/artist':
            builder = (
              BuildContext _,
            ) =>
                Artist(
                  user: arguments["user"],
                );
            break;
          default:
            builder = (
              BuildContext _,
            ) =>
                MyHomePage();
        }

        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List images = [];
  bool loading = false;
  bool searchEnabled = false;
  TextEditingController _textEditingController = TextEditingController();
  ScrollController _scrollController = ScrollController();
  bool firstTime = true;
  String query = "";
  int page = 1;
  double deviceWidth;

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        page += 1;
        _getImages(query);
      }
    });
  }

  void _getImages(String query) {
    firstTime = false;

    if (page == 1) {
      setState(() {
        loading = true;
      });
    }

    getImages(query, page: page.toString()).then((res) {
      setState(() {
        images.addAll(res["results"]);
      });
      loading = false;
    }).then((err) {
      print("Err: $err");
    });
  }

  Widget content() {
    double padding = 5.0;

    if (loading) {
      return LinearProgressIndicator();
    } else {
      if (firstTime) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "So what do you wanna search for?",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.pink[500], fontSize: 16.0),
                )
              ],
            )
          ],
        );
      }
      return Container(
        color: Colors.black,
        child: Padding(
          padding: EdgeInsets.fromLTRB(padding, padding, padding, 0.0),
          child: StaggeredGridView.countBuilder(
            controller: _scrollController,
            // set column count
            crossAxisCount: 2,
            itemCount: images.length,
            // set itemBuilder
            itemBuilder: (BuildContext context, int index) {
              UnsplashImage unsplashImage =
                  UnsplashImage.fromMap(images[index]);

              return ImageContainer(
                unsplashImage: unsplashImage,
                onTap: () {
                  Navigator.of(context).pushNamed("/artist",
                      arguments: {"user": unsplashImage.user});
                },
              );
            },
            staggeredTileBuilder: (int index) {
              UnsplashImage unsplashImage =
                  UnsplashImage.fromMap(images[index]);
              double aspectRatio = unsplashImage.height.toDouble() /
                  unsplashImage.width.toDouble();
              double columnWidth = MediaQuery.of(context).size.width / 2;

              return StaggeredTile.extent(1, aspectRatio * columnWidth);
            },
            mainAxisSpacing: padding + 1.0,
            crossAxisSpacing: padding,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    deviceWidth = MediaQuery.of(context).size.width / 2;

    Widget textField = TextField(
      autofocus: true,
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: "Thinking...",
        hintStyle: TextStyle(
          color: Colors.pink[900],
        ),
      ),
      cursorColor: Colors.pink[500],
      style: TextStyle(
        color: Colors.pink[500],
        fontSize: 19.0,
      ),
      controller: _textEditingController,
      onSubmitted: (text) {
        if (text.isNotEmpty) {
          query = text;
          page = 1;
          images = [];

          _getImages(text);
        }
      },
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: searchEnabled
            ? textField
            : Text(
                "Image Search",
                style: TextStyle(
                  color: Colors.pink[500],
                ),
              ),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                searchEnabled ? Icons.close : Icons.search,
                color: Colors.pink[500],
              ),
              onPressed: () {
                if (searchEnabled) {
                  if (_textEditingController.text.isEmpty) {
                    setState(() {
                      searchEnabled = false;
                    });
                  } else {
                    _textEditingController.clear();
                  }
                } else {
                  setState(() {
                    searchEnabled = true;
                  });
                }
              }),
        ],
      ),
      body: content(),
    );
  }
}
