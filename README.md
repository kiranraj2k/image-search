# Image Search

Am image search app which makes use of the unsplash api.

## Screenshot
![](demo.gif)

## Usage

Make sure you have Flutter installed on your local machine. For more instructions on how to install flutter, look [here](https://flutter.io/docs/get-started/install).
```
https://gitlab.com/kiranraj2k/image-search.git
cd imagesearch
flutter run
```
