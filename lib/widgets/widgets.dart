import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:imagesearch/models/models.dart';

class ImageContainer extends StatefulWidget {
  ImageContainer({this.unsplashImage, this.onTap});

  UnsplashImage unsplashImage;
  Function onTap;

  @override
  _ImageContainerState createState() => _ImageContainerState();
}

class _ImageContainerState extends State<ImageContainer> {
  bool showOverlay = false;
  UnsplashImage _unsplashImage;

  Widget content() {
    Widget overlayWidget = AnimatedContainer(
      duration: Duration(seconds: 1),
      color: showOverlay ? Colors.black87 : Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          AnimatedDefaultTextStyle(
            child: Text(
              widget.unsplashImage.user.name,
            ),
            style: TextStyle(
              fontSize: showOverlay ? 17.0 : 0.0,
              color: Colors.pink[500],
              fontWeight: FontWeight.bold,
            ),
            duration: Duration(milliseconds: 100),
            textAlign: TextAlign.center,
          ),
          AnimatedOpacity(
            opacity: showOverlay ? 1.0 : 0.0,
            duration: Duration(seconds: 1),
            child: Padding(
              padding: EdgeInsets.only(
                top: 5.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.favorite,
                    color: Colors.pink[500],
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      left: 5.0,
                    ),
                    child: Text(
                      _unsplashImage.likes.toString(),
                      style: TextStyle(
                        fontSize: 17.0,
                        color: Colors.pink[500],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Stack(
      fit: StackFit.expand,
      alignment: Alignment.center,
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(3.0),
          child: Image.network(
            widget.unsplashImage.url,
            fit: BoxFit.cover,
          ),
        ),
        overlayWidget,
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    _unsplashImage = widget.unsplashImage;

    return GestureDetector(
      key: new ObjectKey(_unsplashImage.id.toString()),
      onLongPress: () {
        setState(() {
          showOverlay = true;
        });
      },
      onLongPressEnd: (e) {
        setState(() {
          showOverlay = false;
        });
      },
      onTap: widget.onTap,
      child: content(),
    );
  }
}
