class UnsplashImage {
  String id;
  String url;
  int likes;
  User user;
  int width;
  int height;

  UnsplashImage({
    this.id,
    this.url,
    this.likes,
    this.width,
    this.height,
    this.user,
  });

  factory UnsplashImage.fromMap(Map<String, dynamic> json) => UnsplashImage(
        id: json["id"] == null ? null : json["id"],
        url: json["urls"] == null ? null : json["urls"]["small"],
        likes: json["likes"] == null ? null : json["likes"],
        width: json["width"] == null ? null : json["width"],
        height: json["height"] == null ? null : json["height"],
        user: json["user"] == null ? null : User.fromMap(json["user"]),
      );
}

class User {
  String id;
  String username;
  String name;

  User({
    this.id,
    this.name,
    this.username,
  });

  factory User.fromMap(Map<String, dynamic> json) => User(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        username: json["username"] == null ? null : json["username"],
      );
}
